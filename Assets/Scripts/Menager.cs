﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Menager : MonoBehaviour
{

    public float maxDistance = 4.0f;
    public LineRenderer lineRenderer;

    Transform slingshotPos;
    SpringJoint2D springJoint2D;
    Rigidbody2D rigidBody;
    Ray SlingAmmoRay;
    Vector2 prevVelocity;
    bool ClickFlag;
    float AmmoRadious=2.5f;

    // Start is called before the first frame update
    private void Awake()
    {

        springJoint2D = GetComponent<SpringJoint2D>();
        rigidBody = GetComponent<Rigidbody2D>();
        slingshotPos = springJoint2D.connectedBody.transform;
    }
    void Start()
    {
        lineRenderer.SetPosition(0, lineRenderer.transform.position);
        lineRenderer.sortingLayerName = "Default";
        lineRenderer.sortingOrder = 2;
        SlingAmmoRay = new Ray(lineRenderer.transform.position, Vector3.zero);
    }

    // Update is called once per frame
    void Update()
    {
        if (ClickFlag)
            Drag();

        if (springJoint2D != null)
        {
            if (rigidBody.isKinematic && prevVelocity.sqrMagnitude > rigidBody.velocity.sqrMagnitude)
            {
                Destroy(springJoint2D);
                rigidBody.velocity = prevVelocity;
            }
            if (!ClickFlag)
                prevVelocity = rigidBody.velocity;
            LineRendererUpdate();
        }
        else
        {
            lineRenderer.enabled = false;
        }
    }

    private void OnMouseDown()
    {
        springJoint2D.enabled = false;
        ClickFlag = true;
        
    }
    private void OnMouseUp()
    {
        springJoint2D.enabled = true;
        rigidBody.isKinematic = false;
        ClickFlag = false;
    }
    private void Drag()
    {
        Vector3 mausePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        mausePosition.z = 0f;

        Vector2 MauseToSlingshot = mausePosition - slingshotPos.position;
        if(MauseToSlingshot.sqrMagnitude>maxDistance*maxDistance)
        {
            mausePosition.Normalize();
            mausePosition *= maxDistance;
        }

        transform.position = mausePosition;
    }
    void LineRendererUpdate()
    {
        Vector2 SlingToAmmo = transform.position - lineRenderer.transform.position;
        SlingAmmoRay.direction = SlingToAmmo;
        Vector3 RubberHold = SlingAmmoRay.GetPoint(SlingToAmmo.magnitude);
        lineRenderer.SetPosition(1, RubberHold);

    }
}
